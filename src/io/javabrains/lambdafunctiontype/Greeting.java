package io.javabrains.lambdafunctiontype;

public interface Greeting {

    void perform();

}
