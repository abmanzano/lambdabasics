package io.javabrains.lambdafunctiontype;

public class Greeter {

    public static void main(String[] args) {

        // Classic OOP approach where the implementation of perform() method depends on the object passed as an argument
        // to the greet method
        Greeter greeter = new Greeter();
        HelloWorldGreeting hwg = new HelloWorldGreeting();
        NeighbourhoodGreeting ng = new NeighbourhoodGreeting();
        greeter.greet(hwg);
        greeter.greet(ng);

        // Step 2: Using lambda to do the same as above. We need to create an interface with the same return type as the
        // lambda expression
        MyLambda myLambdaFunction = () -> System.out.println("Hello world!");

        MyAdd addFunction = (int a, int b) -> a + b;
        
    }

    public void greet(Greeting greeting) {
        greeting.perform();
    }

}

interface MyLambda {
    void foo();
}

interface MyAdd {
    int add(int a, int b);
}
