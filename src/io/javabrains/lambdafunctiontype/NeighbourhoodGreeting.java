package io.javabrains.lambdafunctiontype;

public class NeighbourhoodGreeting implements Greeting {

    @Override
    public void perform() {
        System.out.println("Hello neighbourhood!");
    }

}
